# Aula4

**Fundamentos do teste de Software** 

**1 - Como gerar qualidade no produto?**

A atividade de testar engobla varios objetos, que juntos geral a qualidade do produto .

* Requisitos atendidos
* Confiança
* Identificar defeitos
* Tomadas de decisão
* Reduzir riscos
* Conformidades contratuais e regulatórias

**Gerar qualidader não é somente testar o Software, a principal preocupação seria garantir que a solicitação do cliente está sendo realizada pela aplicação.**

**Teste dinâmico:**
* Necessita que o Software seja testado
* É o mais usado no mercado
* E é mais caro

**Teste estático:**
* Revisão, inspeção e analise e estatica dos artefatos
* Qualquer documento do projeto pode ser avaliado dessa forma

**O desafio do teste de Software é alcançar, eficácia e eficiência, o teste que encontra defeitos cria oportunidade de melhorar a qualidade do produto.**

**ERRO, DEFEITO e FALHA**
* Um Software pode ter defeitos e nunca falhar
* Falha é um evento
* Defeito é um estaod de Software causado por um erro

**Os Setes Princípios de Testes:**

**1- O teste mostra a presença de defeitos!**
* Testar não é provar que o Software esta correto

**2- Testes exaustivos são impossiveis!**
* Testes exaustivo custam muito caro e tomam muito tempo e são impossíveis

**3- O teste inicial economiza tempo e dinheiro**
* Quanto antes iniciar o teste, maior a probabilidade de não ocorrer falhas

**4- Defeitos se agrupam!**
* Um pequeno numero de modulos contem a maior parte dos defeitos

**5- Cuidado com o paradoxo do pesticida!**
* Os testes precisam ser atualizados sempre, senão se tornam ineficazes

**6- O teste depende do contexto** 
* O teste precisa ser realizado de forma diferente em cada tipo de aplicativo e dominio

**7- Ausência de erros é uma ilusão, pois sempre havera algo que os usuarios podem corrigir!**

**MODELAGEM DO TESTE!!**

1- Planejamento do teste 

2- Análise do teste

3- Modelagem do teste

4- Implementação do teste

5- Execução do teste

6- Conclusão do teste

**Esta modelagem se segue de ordem crscente!**

**Teste de Software é um processor para assegurar um teste eficaz.**





